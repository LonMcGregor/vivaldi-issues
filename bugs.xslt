<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
    <title>Bug List</title>
  </head>
  <body>
    <h1>Bugs</h1>
    <xsl:for-each select="Bugs/Bug">
        <details>
        <summary><h2><xsl:value-of select="Title"/></h2></summary>
        <p><xsl:value-of select="Date" /></p>
        <p><xsl:value-of select="Summary" /></p>
        <p><xsl:value-of select="Key" /></p>
        <p><xsl:value-of select="OS" /></p>
        <p><xsl:value-of select="Version" /></p>
        <p><xsl:value-of select="UserAgent" /></p>
        <p><xsl:value-of select="Steps" /></p>
        <p><xsl:value-of select="Expected" /></p>
        <p><xsl:value-of select="Actual" /></p>
        <p><xsl:value-of select="Responses" /></p>
        </details>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>