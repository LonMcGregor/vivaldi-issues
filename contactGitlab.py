from datetime import datetime as d
import requests as r
import xml.etree.ElementTree as et


GITLAB_API = 'https://gitlab.com/api/v4'
PID = 'LonMcGregor%2Fvivaldi-issues'
with open('token') as tokenfile:
    TOKEN = tokenfile.read().strip()


root = et.parse('allbugs.xml').getroot()

for child in root:
    dt = child.find('Date').text
    dateobj = d.strptime(dt, "%a %d/%m/%Y %H:%M")
    date = dateobj.isoformat()
    title = child.find('Title').text
    body = ''
    body += child.find('OS').text + '\n\n'
    body += child.find('Version').text + '\n\n'
    body += child.find('UserAgent').text + '\n\n'
    body += child.find('Steps').text + '\n'
    body += child.find('Expected').text + '\n'
    body += child.find('Actual').text + '\n'
    key = child.find('Key').text.replace('Key: VB-', '')
    responses = child.find('Responses')

    createdIssueResponse = r.post(
        GITLAB_API+'/projects/'+PID+'/issues',
        headers={'Private-Token': TOKEN},
        data=[
            ('title', title),
            ('iid', key),
            ('description', body),
            ('created_at', date)
        ]
    )

    if createdIssueResponse.status_code != 201:
        input(str(createdIssueResponse.status_code) + 'Failed to create issue '+key+' (maybe it already exists). Press enter to continue to next issue...')
        continue

    if responses is not None:
        input(str(createdIssueResponse.status_code) + 'Created issue '+key+'. Press enter to continue to add notes...')

        for response in responses:
            responsebody = response.find('Author').text + '\n'
            responsebody += response.find('Body').text
            responsebody = responsebody.replace('\n', '\n\n')
            responsedateobj = d.strptime(response.find('Date').text, "%a %d/%m/%Y %H:%M")
            responsedate = responsedateobj.isoformat()

            createdNote = r.post(
                GITLAB_API+'/projects/'+PID+'/issues/'+key+'/notes',
                headers={'Private-Token': TOKEN},
                data=[
                    ('body', responsebody),
                    ('description', """content over
            multiple lines"""),
                    ('created_at', responsedate)
                ]
            )
            input(str(createdNote.status_code) + 'Created issue note. Press enter to continue to next...')
    else:
        input(str(createdIssueResponse.status_code) + 'Created issue '+key+'. Press enter to continue to next...')


exit(0)
