import requests as r
import re
from datetime import datetime as d

GITLAB_API = 'https://gitlab.com/api/v4'
PID = 'LonMcGregor%2Fvivaldi-issues'
with open('token') as tokenfile:
    TOKEN = tokenfile.read().strip()
MAIL_SEP = '$'

def getLongInput(prompt):
    s = ''
    lastLine = input(prompt+' End with $ on its own line.\n')
    while lastLine != MAIL_SEP:
        s += lastLine + '\n'
        lastLine = input()
    return s

date = input('What is the date? Blank input for today, otherwise a date in the form Ddd DD/MM/YYYY HH:MM > ')
trydate = None
try:
    trydate = d.strptime(date, "%a %d/%m/%Y %H:%M")
except:
    trydate = None
while date != '' and trydate is None:
    date = input('What is the date? Blank input for today, otherwise a date in the form Ddd DD/MM/YYYY HH:MM > ')
    trydate = None
    try:
        trydate = d.strptime(date, "%a %d/%m/%Y %H:%M")
    except:
        trydate = None
if date != '':
    date = trydate.isoformat()

key = input('VB-XXXXXX number without leading VB: ')
while not key.isdigit():
    key = input('VB-XXXXXX number: ')

title = input('Brief summary of bug: ')
while title == '':
    title = input('Brief summary of bug: ')
title = '(VB-'+key+') ' + title

body = getLongInput('Paste the whole mail here. End with a single $.')

data=[
    ('title', title),
    ('iid', key),
    ('description', body)
]

if date != '':
    data.append(('created_at', date))

createdIssue = r.post(
    GITLAB_API+'/projects/'+PID+'/issues',
    headers={'Private-Token': TOKEN},
    data=data
)
