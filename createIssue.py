import requests as r
from datetime import datetime as d

GITLAB_API = 'https://gitlab.com/api/v4'
PID = 'LonMcGregor%2Fvivaldi-issues'
with open('token') as tokenfile:
    TOKEN = tokenfile.read().strip()

def getLongInput(prompt):
    s = ''
    lastLine = input(prompt+' End with $ on its own line.\n')
    while lastLine != '$':
        s += lastLine + '\n'
        lastLine = input()
    return s

key = input('VB-XXXXXX number without leading VB: ')
while not key.isdigit():
    key = input('VB-XXXXXX number: ')

title = input('Brief summary of bug: ')
while title == '':
    title = input('Brief summary of bug: ')
title = '(VB-'+key+') ' + title

date = input('What is the date? Blank input for today, otherwise an ISO date: ')
trydate = None
try:
    trydate = d.fromisoformat(date)
except:
    trydate = None
while date != '' and trydate is None:
    date = input('What is the date? Blank input for today, otherwise an ISO date: ')
    trydate = None
    try:
        trydate = d.fromisoformat(date)
    except:
        trydate = None

os = input('What is the OS?: ')
while not os.startswith('Windows') and not os.startswith('Linux'):
    os = input('What is the OS?: ')
os = '*Operating System:* ' + os

version = input('What is the Version?: ')
while not version.startswith('Vivaldi version: '):
    version = input('What is the Version?: ')
version = '*Environment:* ' + version

ua = input('What is the User agent?')
while not ua.startswith('Mozilla'):
    ua = input('What is the User agent?')
ua = '*User agent:* ' + ua

steps = '*Steps to reproduce:*\n\n' + getLongInput('What are the steps?')
url = input('Affected URL (if any): ')
if url != '':
    url = '*Affected URL:* ' + url
expected = '*Expected behaviour:*\n\n' + getLongInput('What are the expected behaviour?')
actual = '*Actual behaviour:*\n\n' + getLongInput('What are the actual behaviour?')

body = os+'\n\n'+version+'\n\n'+ua+'\n\n'+steps+'\n\n'+expected+'\n\n'+actual
if url != '':
    body += '\n\n*Affected URL:* ' + url

print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")
print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")
print(title, '\n', body)
print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")
input('is this the correct bug? ^C to quit now')

data=[
    ('title', title),
    ('iid', key),
    ('description', body)
]

if date != '':
    data.append(('created_at', date))

createdIssue = r.post(
    GITLAB_API+'/projects/'+PID+'/issues',
    headers={'Private-Token': TOKEN},
    data=data
)
