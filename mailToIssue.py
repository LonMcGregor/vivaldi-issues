import requests as r
import re
from datetime import datetime as d

GITLAB_API = 'https://gitlab.com/api/v4'
PID = 'LonMcGregor%2Fvivaldi-issues'
with open('token') as tokenfile:
    TOKEN = tokenfile.read().strip()
MAIL_SEP = '-------------------------------------------------------------------------------'

def getLongInput(prompt):
    isFirstLine = True
    s = ''
    lastLine = input(prompt+' End with $ on its own line.\n')
    while isFirstLine or lastLine != MAIL_SEP:
        isFirstLine = False
        s += lastLine + '\n'
        lastLine = input()
    return s


date = input('What is the date? Blank input for today, otherwise a date in the form Ddd DD/MM/YYYY HH:MM > ')
trydate = None
try:
    trydate = d.strptime(date, "%a %d/%m/%Y %H:%M")
except:
    trydate = None
while date != '' and trydate is None:
    date = input('What is the date? Blank input for today, otherwise a date in the form Ddd DD/MM/YYYY HH:MM > ')
    trydate = None
    try:
        trydate = d.strptime(date, "%a %d/%m/%Y %H:%M")
    except:
        trydate = None
if date != '':
    date = trydate.isoformat()

mail = getLongInput('Paste the whole mail here between each ------ separator.')

key = re.search(r'Key: VB-(\d+)', mail).group(1)
title = '(VB-'+key+') ' + re.search(r'Summary: (.+)', mail).group(1)
os = re.search(r'Operating System: .+', mail).group(0)
version = re.search(r'Environment: .+', mail).group(0)
ua = re.search(r'User Agent: .+', mail).group(0)

pos = mail.find('*Steps to reproduce:*')
details = mail[pos:]
details = details.replace(':*', ':*\n')

body = os+'\n\n'+version+'\n\n'+ua+'\n\n'+details

print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")
print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")
print(title, '\n', body)
print("=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=")

input("Is this OK?")

data=[
    ('title', title),
    ('iid', key),
    ('description', body)
]

if date != '':
    data.append(('created_at', date))

createdIssue = r.post(
    GITLAB_API+'/projects/'+PID+'/issues',
    headers={'Private-Token': TOKEN},
    data=data
)
