# vivaldi-issues

## If you have a bug to report use the official reporter at https://vivaldi.com/bugreport

My tracker for reported vivaldi issues.

The aim is to mirror in a public fashion bugs that I have found with the vivaldi bug tracker.


Please don't file new bugs, as I use the given scripts to give them the correct bug IDs. But you can let me know on the forum or discord.

Project image by [boaworm](https://commons.wikimedia.org/wiki/File:Fimmvorduhals_second_fissure_2010_04_02.JPG)